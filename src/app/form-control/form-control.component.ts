import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HEROES } from '../mock-heroes';

@Component({
  selector: 'app-form-control',
  templateUrl: './form-control.component.html',
  styleUrls: ['./form-control.component.css']
})
export class FormControlComponent implements OnInit {
  heroList = HEROES;
  name2 = new FormControl('');

  profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl('')
  });

  profileForm2 = this.fb.group({
    firstName2: ['', Validators.compose([Validators.required, Validators.email])],
    lastName2: [''],
    address: this.fb.group({
      street: [''],
      city: [''],
      state: [''],
      zip: ['']
    }),
  });

  formHero = this.fb.group({
    id: [this.idProximo],
    name: ['', Validators.required]
  });

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
  }

  updateName(): void{
    this.name2.setValue('Bernardo');
  }
  onSubmit(): void{
    console.warn(this.profileForm.value);
  }
  onSubmit2(): void{
    console.log(this.profileForm2.value);
  }
  addHero(): void{
    // for (let x = 0; x < this.heroList.length; x++)
    for (const hero of this.heroList){
      if (hero.id === this.formHero.get('id').value){
        hero.name = this.formHero.get('name').value;
        this.formHero.get('name').reset();
        this.formHero.get('id').setValue(this.idProximo);
        return alert('HEROI EDITADO');
      }
    }
    this.heroList.push(this.formHero.value);
    this.formHero.get('name').reset();
    this.formHero.get('id').setValue(this.idProximo);
  }
  get idProximo(): number{
    return this.heroList[this.heroList.length - 1 ].id + 1;
  }
  editHero(value: any): void {
    console.log(value);
    this.formHero.get('name').setValue(value.name);
    this.formHero.get('id').setValue(value.id);
  }
}
