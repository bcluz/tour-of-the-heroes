import { Component, OnInit} from '@angular/core';
import {MessageService} from '../message.service';
import {HEROES} from '../mock-heroes';


@Component({
  selector: 'app-selection-list',
  templateUrl: './selection-list.component.html',
  styleUrls: ['./selection-list.component.css']
})
export class SelectionListComponent implements OnInit {
  heroes = HEROES;
  
  constructor(private messageService: MessageService) { 
    messageService.add('Entered Challenge List page!');
  }

  ngOnInit(): void {
  }
  //NAO FUNCIONA
  // func(eventSelected:any){
    //   let found:Hero = this.heroes.find(hero => hero.name == eventSelected)
    //   let saiu = this.heroes.slice(this.heroes.indexOf(found),1)
    //   console.log(saiu)
    // }

    //NAO FUNCIONA
    // func(eventSelected:Hero){
      //   let found:Hero = this.heroes.find(hero => hero.name == eventSelected)
      //   return (eventSelected != found)
      // }
  //FUNCIONA mas é gambiarra
  // func(eventSelected:any){
  //   for(let x =0;x< this.heroes.length;x++){
  //     if(this.heroes[x] == eventSelected){
  //       let trocar = this.heroes[0]
  //       this.heroes[0] = this.heroes[x]
  //       this.heroes[x] = trocar
  //       this.heroes.shift();
  //     }
  //   }
  //    console.log(`O Heroi [${eventSelected.name}] Foi excluido com Sucesso!`);
  // }
  
  //Funciona do melhor jeito
  delete(eventSelected:any){ 
    this.heroes = this.heroes.filter(hero => hero.name !== eventSelected.name);// PQ DIABOS NAO ARMAZENA -> res: eu tinha esquecido do return na funçao! por isso nao retornou
    console.log(`O Heroi [${eventSelected.name}] Foi excluido com Sucesso!`);
  }
}
