import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import {MessageService} from '../message.service';

@Component({
  selector: 'app-estudos',
  templateUrl: './estudos.component.html',
  styleUrls: ['./estudos.component.css']
})
export class EstudosComponent implements OnInit, OnDestroy {
  teste = '';
  lista = [];


  constructor(private messageService: MessageService) { messageService.add('Entered Estudos page!'); }

  ngOnInit(): void {
    console.log('Pagina Chamada com sucesso');
  }
  ngOnDestroy(): void {
    console.log('Pagina Destruida com sucesso');
  }

  funTest(txt: string): void{
    this.teste = txt;
  }

  addToList(arg: any): void{
    this.lista.push(arg.value);
    arg.value = '';
  }
  show(): void{
    console.log('event');
  }

}
